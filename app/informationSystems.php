<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class informationSystems extends Model
{
    
     /* @var array
     */
    protected $fillable = [
        'systemName', 'systemAbbreviation', 'url', 'systemDescription', 'frontEnd', 'backEnd', 'computingScheme', 'developmentStrategy', 'systemAdministrators', 'endUsers', 'ictsFocalPersons', 'status', 'dateDeveloped', 'dateCompleted',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
