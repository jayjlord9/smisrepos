<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ISmodel extends Model
{
     protected $guarded=[];

    public function tasks(){

    	return $this->hasMany(IStask::class);
    }
}
