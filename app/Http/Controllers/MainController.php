<?php

namespace App\Http\Controllers;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\account;
use App\User;
use DB;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Auth;
use App\informationSystems;
use App\ISmodel;


class MainController extends Controller
{
	 public function checkLogin(Request $request){

		$this->validate($request, [
			'password' => 'required|min:3'
		]);

			$userData = $request->only('username', 'password');

			if (Auth::attempt($userData)){

				return redirect()->intended('main/dashboard');
			}
			else{
			return back()->with('error','wrong login details');
				}
		}
	//public function profileModal(){

	//	 $users = DB::table('users')->select('name')->get();

   //     return view('profileModal')->with('users', $users);

	//}
		//$userData = array(
		//'email' => $request->get('email'),
		//'password' => $request->get('password'));
		//dd($userData);
		//if(Auth::attempt($userData)){
		//	return redirect()->intended('main/successLogin');
		//}
		//else{
		//	return back()->with('error','wrong login details');
		
	

	  public function dashboard(){

		return view('dashboard');

	}

	  public function logout(){
			Auth::logout();
			return redirect('/');
	}

	public function create()
    {
    	return view('page-register');
    }


    public function store(Request $request)
    {
        $this->validate(request(),[
            'name' => 'required',
            'username' => 'required',
            'email'=> 'required',
            'password' => 'required'

            ]);         
       
    $user= new User();
    
    	$user->name= $request['name'];
        $user->username= $request['username'];
        $user->email= $request['email'];
        $user->password= Hash::make($request['password']);
        $user->remember_token=str_random(10);
    // add other fields
    $user->save();
        return redirect('main/page-register');
    }

	 public function forget(){
		return view('pages-forget');
	}

	 public function profile(){
		return view('profile');
	}


	public function ISstore(Request $request){
		      
				       
				    $is= new App\informationSystems();
				    
				    	$is->systemName= $request['systemName'];
				        $is->systemAbbreviation= $request['systemAbbreviation'];
				        $is->url= $request['url'];
				        $is->systemDescription= $request['systemDescription'];
				        $is->frontEnd= $request['frontEnd'];
				        $is->backEnd= $request['backEnd'];
				        $is->computingScheme= $request['computingScheme'];
				        $is->developmentStrategy= $request['developmentStrategy'];
				        $is->systemAdministrators= $request['systemAdministrators'];
				        $is->endUsers= $request['endUsers'];
				        $is->ictsFocalPersons= $request['ictsFocalPersons'];
				        $is->status= $request['status'];
				        $is->dateDeveloped= $request['dateDeveloped'];
				        $is->dateCompleted= $request['dateCompleted'];

				    // add other fields
				    $is->save();
				        return redirect('main/informationSystem');
	}

	public function view($id){
		//$ISdata = informationSystems::where('id',$data->id);
	//return view('view',['system'=>$ISdata]);
		$datas = informationSystems::find($id);
		return view('view', compact('datas'));
		//return view('view', compact('data'));
	}

	public function show(informationSystems $id){
		//$ISdata = DB::select('select * from information_Systems');
		//return view('informationSystem')->with('link',$ISdata)->with('results', $results);

		$ISdata = informationSystems::all();
			
    	return view('informationSystem', compact('ISdata'));

	}

	public function edit($id){

		$editIS = App\informationSystems::find($id);
		return view('informationSystem', compact('editIS', 'id'));
	}

	public function update(Request $request, $id){

			$isEdit = App\informationSystems::find($id);

			$isEdit->systemName= $request->get('systemName');
	        $isEdit->systemAbbreviation= $request->get('systemAbbreviation');
	        $isEdit->url= $request->get('url');
	        $isEdit->systemDescription= $request->get('systemDescription');
	        $isEdit->frontEnd= $request->get('frontEnd');
	        $isEdit->backEnd= $request->get('backEnd');
	        $isEdit->computingScheme= $request->get('computingScheme');
	        $isEdit->developmentStrategy= $request->get('developmentStrategy');
	        $isEdit->systemAdministrators= $request->get('systemAdministrators');
	        $isEdit->endUsers= $request['endUsers'];
	        $isEdit->ictsFocalPersons= $request->get('ictsFocalPersons');
	        $isEdit->status= $request->get('status');
	        $isEdit->dateDeveloped= $request->get('dateDeveloped');
	        $isEdit->dateCompleted= $request->get('dateCompleted');

	        $isEdit->save();
	        return redirect()->route('informationSystem')->with('success', 'Data Updated');
	}

	public function destroy(){

	}


}