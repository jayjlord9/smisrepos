<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});



Route::post('main/checkLogin', 'MainController@checkLogin');
Route::get('main/dashboard', 'MainController@dashboard');
Route::get('main/logout', 'MainController@logout');
Route::get('main/page-register', 'MainController@create');
Route::get('main/pages-forget', 'MainController@forget');
Route::get('main/profile', 'MainController@profile'); 
Route::get('main/profileModal', 'MainController@profileModal');
Route::post('main/page-register', 'MainController@store');
//Route::get('main/informationSystem', 'MainController@informationSystem');
Route::post('main/informationSystem', 'MainController@ISstore');
Route::get('main/informationSystem', 'MainController@show');
//Route::get('main/informationSystem', 'MainController@view');
Route::post('main/informationSystem', 'MainController@edit');
Route::get('view/{id}', 'MainController@view');