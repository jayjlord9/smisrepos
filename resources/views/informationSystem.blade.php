@extends('dashboard')

@section('content')
	
	
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Information System List</strong>

                                <button class="btn btn-primary float-right rounded fa fa-plus-square" data-toggle="modal" data-target="#addIS"> Add</button>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>System Name</th>
                                            <th>System Abreviation</th>
                                            <th>URL</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($ISdata as $data)
                                        <tr>
                                            <td >{{$data->id}}</td>
                                            <td>{{$data->systemName}}</td>
                                            <td>{{$data->systemAbbreviation}}</td>
                                            <td><a href="#" target="_blank">{{$data->url}}</a></td>
                                            <td>
                                              <a href="{{action('MainController@view', $data['id'])}}" class="btn btn-primary rounded">view </a> 

                                              <a  class="btn btn-success rounded" data-toggle="modal" data-target="#editIS">edit</a>
                                            </td>
                        
                                        </tr>
                                      @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div><!-- .animated -->

<!--MODALS-->

      <!--Add Information System Modal-->
             <div class="modal fade" id="addIS" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="mediumModalLabel">ADD Information System</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                             <form method="POST">
                            <div class="modal-body">
                              
                               		{{csrf_field()}}
                               		<div class="form-group">
                               			<label><strong>System Name</strong> </label>
                               			<input type="text" class="form-control" name="systemName" placeholder="Enter System Name">
                               		</div>

                               		<div class="form-group">
                               			<label><strong> System Abbreviation</strong></label>
                               			<input type="text" name="systemAbbreviation" class="form-control" placeholder="Enter System Abbreviation">
                               		</div>
                               		<div class="form-group">
                               			<label><strong> URL</strong></label>
                               			<input type="text" name="url" class="form-control" placeholder="Enter URL">
                               		</div>
                               		<div class="form-group">
                               			<label><strong> Description</strong></label>
                               			<textarea name="systemDescription" class="form-control" placeholder="Description"></textarea>
                               		</div>
                               		<div class="form-group">
                               			<label><strong>Technologies Used</strong> </label>
                               			<div class="input-group">
                               				<input type="text" name="frontEnd" placeholder="Enter Front-End" class="form-control">
                               				<input type="text" name="backEnd" placeholder="Enter Back-End" class="form-control">				
                               			</div>
                               			
                               		</div>
                               		<div class="form-group">
                               			<label><strong>Computing Scheme</strong> </label>
                               			<input name="computingScheme" class="form-control" placeholder="Enter Computing Scheme: ex. Web Base"></input>
                               		</div>

                               		<div class="form-group">
                               			<label><strong>Development Strategy</strong> </label>
                               			<input name="developmentStrategy" class="form-control" placeholder="Enter Development Strategy"></input>
                               		</div>
                               		<div class="form-group">
                               			<label><strong> System Administrators</strong></label>
                               			<input type="text" name="systemAdministrators" class="form-control" placeholder="Enter System Administrators">
                               		</div>
                               		<div class="form-group">
                               			<label><strong>End User</strong> </label>
                               			<input type="text" name="endUsers" class="form-control" placeholder="Enter End USer">
                               		</div>
                               		<div class="form-group">
                               			<label><strong>ICTS Focal Person/s</strong> </label>
                               			<input type="text" name="ictsFocalPersons" class="form-control" placeholder="Enter ICTS Focal Person/s">
                               		</div>
                               		<div class="form-group">
                               			<label><strong>Status</strong> </label>
                               			<input type="text" name="status" class="form-control" placeholder="Enter Status">
                               		</div>
                               		<div class="form-group">
                               			<label><strong>Date Developed</strong> </label>
                               			<input id="datepicker" type="text" name="dateDeveloped" class="form-control" placeholder="Enter Date Developed">
                               		</div>
                               		<div class="form-group">
                               			<label><strong>Date Completed</strong> </label>
                               			<input id="datepickers" type="text" name="dateCompleted" class="form-control" placeholder="Enter Date Developed">
                               		</div>


                              
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                             </form>
                        </div>
                    </div>
                </div>
      <!-- END Add Information System Modal-->
  
     


<!--End Modal-->
@endsection