<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Software Management and Information System</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="../vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <link rel="stylesheet" type="text/css" href="../css/landing-page.min.css">

  </head>

  <body class="body-background">
   <div class="login">

    <div class="logo-div">
        <img src="img/neda.png" class="rounded logo" alt="sareh">
        <img src="img/pilipinas.png" class="rounded logo" alt="sareh">
    </div>

    <center><label style="font-family: Georgia, serif;">National Economic and Development Autority</label></center>
        @if(isset(Auth::user()->username))
            <script> window.location="main/dashboard";</script>

          @endif

          @if($message = Session::get('error'))
            <div class="alert alert-danger alert-block">

              <strong>{{$message}}</strong>
            </div>
           @endif

          @if(count($errors)>0)
              <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
              </div>
          @endif

      <div class="login-form-div container box">
        <div class="container box">

          <form method="post" action="{{url('/main/checkLogin')}}">
            {{csrf_field()}}
                <div class="form-group">
                    <label>Enter Username</label>
                    <input type="text" name="username" class="form-control">
                </div>

                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control">
                </div>

                <div class="form-group">
                    <input type="submit" name="login" value="Login" class="btn btn-primary">
                </div>
          </form>
        </div>
      </div>
  </div> 
 
    <!-- Bootstrap core JavaScript -->
    <script src="../public/vendor/jquery/jquery.min.js"></script>
    <script src="../public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
