@extends('dashboard')

@section('content')

            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title"><h1>{{$datas->systemName}}</h1></strong>
                            </div>
                            <div class="card-body">
                                 
                              <div>
                                <div class="col-5">
                                      <div class=" flex-column">
                                        <label class="flex-row col-3">
                                          <strong>ID:</strong>
                                        </label>
                                       <label class="flex-row col-3">
                                          {{$datas->id}}
                                        </label>
                                      </div>

                                       <div class=" flex-column">
                                         
                                        <label class="flex-row col-3">
                                          <strong>URL:</strong>
                                        </label>
                                        <label class="flex-row col-3">
                                          {{$datas->url}}
                                        </label>
                                      </div>
                                  </div>

                                  <div class="col-7">
                                      <div class="flex-column">
                                        <label class="flex-row col-5">
                                          <strong>System Abbbreviation:</strong>
                                        </label>
                                        <label class="flex-row col-4">
                                          {{$datas->systemAbbreviation}}
                                        </label>
                                      </div>

                                       <div class="flex-column">
                                          <label class="flex-row col-5">
                                            <strong>Computing Scheme:</strong>
                                          </label>
                                          <label class="flex-row col-4">
                                            {{$datas->computingScheme}}
                                          </label>
                                        </div>


                                        <div class="flex-column">
                                          <label class="flex-row col-5">
                                            <strong>Development Strategy:</strong>
                                          </label>
                                          <label class="flex-row col-4">
                                            {{$datas->developmentStrategy}}
                                          </label>
                                        </div>
                                  </div>
                              </div>
                                      
                              <div class="col-12" style="height: 20px;"></div>
                                  <div class="">
                                    <div class="col-4">
                                       
                                        <div>
                                          <label class="col-8">
                                            <strong><center><h5>Technologies Used</h5></center></strong>
                                          </label>
                                        </div> 
                                      
                                          <div class=" d-flex flex-row">
                                            <div class="flex-row">
                                              <label class="d-flex flex-column col-12">
                                                <strong>Front End:</strong>
                                              </label>
                                              <label class="d-flex flex-column col-12">
                                                   {{$datas->frontEnd}}
                                              </label>
                                            </div>

                                            <div class="flex-row">
                                              <label class="d-flex flex-column col-12">
                                                <strong>Back End:</strong>
                                              </label>

                                              <label class="d-flex flex-column col-12">
                                                  {{$datas->backEnd}}
                                              </label>
                                            </div>
                                          </div>
                                     </div>  

                                       

                                      <div class="d-flex flex-row col-8">
                                        <div >
                                          <label class="d-flex flex-column col-12">
                                            <strong>System Administrators:</strong>
                                          </label>
                                          <label class="d-flex flex-column col-12">
                                            {{$datas->systemAdministrators}}
                                          </label>
                                        </div>

                                        <div>
                                          <label class="d-flex flex-column col-12">
                                            <strong>End Users:</strong>
                                          </label>
                                          <label class="d-flex flex-column col-12">
                                            {{$datas->endUsers}}
                                          </label>
                                        </div>

                                        <div>
                                          <label class="d-flex flex-column col-12">
                                            <strong>ICTS Focal Persons:</strong>
                                          </label>
                                          <label class="d-flex flex-column col-12">
                                            {{$datas->ictsFocalPersons}}
                                          </label>
                                        </div>
                                    </div>
                                  </div>  



                                  <div class="col-lg-12">
                                    <div >
                                        <div class="col order-first">
                                          <label class="">
                                            <strong>Status:</strong>
                                          </label>
                                          <label class="">
                                            {{$datas->status}}
                                          </label>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div>
                                          <label class="d-flex flex-row">
                                            <strong>Date Developed:</strong>
                                          </label>
                                          <label class="d-flex flex-row">
                                            {{$datas->dateDeveloped}}
                                          </label>
                                        </div>

                                        <div>
                                          <label class="d-flex flex-row">
                                            <strong>Date Completed:</strong>
                                          </label>
                                          <label class="d-flex flex-row ">
                                            {{$datas->dateCompleted}}
                                          </label>
                                        </div>
                                    </div>
                                  </div>

                                        <div class="form-group col-lg-12">

                                          <div>
                                            <label>
                                              <strong>System Description:</strong>
                                            </label>

                                            <textarea class="form-control z-depth-1">
                                              {{$datas->systemDescription}}
                                            </textarea>
                                          </div>
                                          
                                        </div>



                                      

<!--
                                  <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                   
										                 <tbody>
                                                  <tr>
                                                      <th scope="row">ID:</th>
                                                      <td >{{$datas->id}}</td>
                                                  </tr>
                                                  <tr>
                                                      <th scope="row">System Abbreviation:</th>
                                                      <td>{{$datas->systemAbbreviation}}</td>
                                                  </tr>
                                                  <tr>
                                                      <th scope="row">URL:</th>
                                                      <td></t>
                                                  </tr>
                                                  <tr>
                                                      <th scope="row">System Description:</th>
                                                      <td></t>
                                                  </tr>
                                                  <tr>
                                                      <th scope="row">Front End:</th>
                                                      <td></t>
                                                  </tr>
                                                  <tr>
                                                      <th scope="row">Back End:</th>
                                                      <td></t>
                                                  </tr>
                                                  <tr>
                                                      <th scope="row">Computing Scheme:</th>
                                                      <td></t>
                                                  </tr>
                                                  <tr>
                                                      <th scope="row">Development Strategy:</th>
                                                      <td></t>
                                                  </tr>
                                                  <tr>
                                                      <th scope="row">System Administrators:</th>
                                                      <td></t>
                                                  </tr>
                                                  <tr>
                                                      <th scope="row">End Users:</th>
                                                      <td></t>
                                                  </tr>
                                                  <tr>
                                                      <th scope="row">ICTS Focal Persons:</th>
                                                      <td></t>
                                                  </tr>
                                                  <tr>
                                                      <th scope="row">Status:</th>
                                                      <td></t>
                                                  </tr>
                                                  <tr>
                                                      <th scope="row">Date Developed:</th>
                                                      <td></t>
                                                  </tr>
                                                  <tr>
                                                      <th scope="row">Date Completed:</th>
                                                      <td></t>
                                                  </tr>
                                              </tbody>

                                </table>
-->
                            </div>
                        </div>
                    </div>


                </div>
            </div><!-- .animated -->
 

@endsection