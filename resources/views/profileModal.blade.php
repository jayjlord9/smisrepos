 @extends('dashboard')

 @section('content')               
                <div class="modal fade" id="smallmodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">

                            @foreach($users as $user)
                                <h5 class="modal-title" id="smallmodalLabel">{{$user->name}}</h5>
                             @endforeach   

                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary">EDIT</button>
                            </div>
                        </div>
                    </div>
                </div>
@endsection