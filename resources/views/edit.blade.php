 <!--EDIT MODAL-->
      <!-- 
       <div class="modal fade" id="editIS" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="mediumModalLabel">ADD Information System</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                             <form method="POST" action="">
                            <div class="modal-body">
                              
                                  {{csrf_field()}}
                                  <input type="hidden" name="_method" value="PATCH">
                                  <div class="form-group">
                                    <label><strong>System Name</strong> </label>
                                    <input type="text" class="form-control" name="systemName" value="{{$editIS->systemName}}">
                                  </div>

                                  <div class="form-group">
                                    <label><strong> System Abbreviation</strong></label>
                                    <input type="text" name="systemAbbreviation" class="form-control" value="{{$editIS->systemAbbreviation}}">
                                  </div>
                                  <div class="form-group">
                                    <label><strong> URL</strong></label>
                                    <input type="text" name="url" class="form-control" value="{{$editIS->url}}">
                                  </div>
                                  <div class="form-group">
                                    <label><strong> Description</strong></label>
                                    <textarea name="systemDescription" class="form-control" value="{{$editIS->systemDescription}}"></textarea>
                                  </div>
                                  <div class="form-group">
                                    <label><strong>Technologies Used</strong> </label>
                                    <div class="input-group">
                                      <input type="text" name="frontEnd" value="{{$editIS->frontEnd}}" class="form-control">
                                      <input type="text" name="backEnd" value="{{$editIS->backEnd}}" class="form-control">        
                                    </div>
                                    
                                  </div>
                                  <div class="form-group">
                                    <label><strong>Computing Scheme</strong> </label>
                                    <input name="computingScheme" class="form-control" value="{{$editIS->computingScheme}}"></input>
                                  </div>

                                  <div class="form-group">
                                    <label><strong>Development Strategy</strong> </label>
                                    <input name="developmentStrategy" class="form-control" value="{{$editIS->developmentStrategy}}" ></input>
                                  </div>
                                  <div class="form-group">
                                    <label><strong> System Administrators</strong></label>
                                    <input type="text" name="systemAdministrators" class="form-control" value="{{$editIS->systemAdministrators}}" >
                                  </div>
                                  <div class="form-group">
                                    <label><strong>End User</strong> </label>
                                    <input type="text" name="endUsers" class="form-control" value="{{$editIS->endUsers}}">
                                  </div>
                                  <div class="form-group">
                                    <label><strong>ICTS Focal Person/s</strong> </label>
                                    <input type="text" name="ictsFocalPersons" class="form-control" value="ictsFocalPersons">
                                  </div>
                                  <div class="form-group">
                                    <label><strong>Status</strong> </label>
                                    <input type="text" name="status" class="form-control" value="{{$editIS->status}}">
                                  </div>
                                  <div class="form-group">
                                    <label><strong>Date Developed</strong> </label>
                                    <input id="datepicker" type="text" name="dateDeveloped" class="form-control" value="{{$editIS->dateDeveloped}}">
                                  </div>
                                  <div class="form-group">
                                    <label><strong>Date Completed</strong> </label>
                                    <input id="datepickers" type="text" name="dateCompleted" class="form-control" value="{{$editIS->dateCompleted}}">
                                  </div>


                              
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                             </form>
                        </div>
                    </div>
                </div>
              -->