<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InformationSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information_systems', function (Blueprint $table) {
            $table->increments('id');
            $table->string('systemName');
            $table->string('systemAbbreviation');
            $table->string('url');
            $table->string('systemDescription');
            $table->string('frontEnd');
            $table->string('backEnd');
            $table->string('computingScheme');
            $table->string('developmentStrategy');
            $table->string('systemAdministrators');
            $table->string('endUsers');
            $table->string('ictsFocalPersons');
            $table->string('status');
            $table->date('dateDeveloped');
            $table->date('dateCompleted');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('information_systems');
    }
}
